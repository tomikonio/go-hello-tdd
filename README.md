# go-hello-tdd

A Go project with TDD  
A walkthrough of [Learn Go with Tests](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals) book

## Current location in the book

~~[discipline](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/hello-world#discipline)~~  
~~[Integers](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/integers)~~  
~~[Iteration](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/iteration)~~  
[Arrays and slices](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/arrays-and-slices):

- ~~[Refactor](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/arrays-and-slices#refactor)~~
- ~~[Write the test first](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/arrays-and-slices#write-the-test-first-1)~~
- ~~[Write the test first 2](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/arrays-and-slices#write-the-test-first-2)~~
- [Refactor 4](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/arrays-and-slices#refactor-3)
