package array

// Sum sums up the numbers in the arrray
func Sum(numbers []int) int {
	sum := 0
	for _, number := range numbers {
		sum += number
	}
	return sum
}

// SumAll returns a slice with the sum of each input slice
func SumAll(numbersToSum ...[]int) []int {
	numberOfSlices := len(numbersToSum)
	sums := make([]int, numberOfSlices)

	for i, numbers := range numbersToSum {
		sums[i] = Sum(numbers)
	}

	return sums

}

// SumAllTails returns a slice with the sums of the tails of the inputed arrays
func SumAllTails(numbersToSum ...[]int) []int {
	var sums []int
	for _, numbers := range numbersToSum {
		tail := numbers[1:]
		sums = append(sums, Sum(tail))

	}
	return sums
}
